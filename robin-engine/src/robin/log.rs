use spdlog::prelude::*;

pub struct Log {
    core_logger: Logger,
    client_logger: Logger,
}

impl Log {
    pub fn init() -> Log {
        let sinks = spdlog::default_logger().sinks().to_owned();
        let mut builder = Logger::builder();
        let builder = builder.sinks(sinks).level_filter(LevelFilter::All);

        let core_logger = builder.name("core").build();
        let client_logger = builder.name("client").build();

        Self {
            core_logger,
            client_logger,
        }
    }
    pub fn rb_core_trace(&self, msg: &str) {
        trace!(logger: self.core_logger, "{}", msg);
    }
    pub fn rb_core_info(&self, msg: &str) {
        info!(logger: self.core_logger, "{}", msg);
    }
    pub fn rb_core_warn(&self, msg: &str) {
        warn!(logger: self.core_logger, "{}", msg);
    }
    pub fn rb_core_error(&self, msg: &str) {
        error!(logger: self.core_logger, "{}", msg);
    }
    pub fn rb_core_fatal(&self, msg: &str) {
        critical!(logger: self.core_logger, "{}", msg);
    }

    pub fn rb_client_trace(&self, msg: &str) {
        trace!(logger: self.client_logger, "{}", msg);
    }
    pub fn rb_client_info(&self, msg: &str) {
        info!(logger: self.client_logger, "{}", msg);
    }
    pub fn rb_client_warn(&self, msg: &str) {
        warn!(logger: self.client_logger, "{}", msg);
    }
    pub fn rb_client_error(&self, msg: &str) {
        error!(logger: self.client_logger, "{}", msg);
    }
    pub fn rb_client_fatal(&self, msg: &str) {
        critical!(logger: self.client_logger, "{}", msg);
    }
}
