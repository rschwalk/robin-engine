use crate::robin::log;

pub fn application() {
    println!("Hello Application!");
    let logger = log::Log::init();
    logger.rb_core_warn("Initialized Log!");
    let var: u8 = 3;
    logger.rb_client_info(&format!("Var is: {}", var));
}
