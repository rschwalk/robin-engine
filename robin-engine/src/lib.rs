mod robin;

pub fn hello() {
    println!("Hello from Robin!");
    robin::application::application();
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
